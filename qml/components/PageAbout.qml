/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

Page {
    id: aboutPage

    anchors.fill: parent

    header: PageHeader {
        id: aboutHeader
        title: i18n.tr('About')
        flickable: mainflick
    }

    property string iconAppRute: "../../assets/logo.svg"
    property string source: "<a href='https://gitlab.com/cibersheep/rock-paper-scissors-lizard-spock'>Gitlab</a>"
    property string license: "<a href='https://www.gnu.org/licenses/gpl-2.0.html'>GPLv2</a>"
    property string darkColor: "#3d57b8"
    property string selectionColor: "#333d57b8"

    ListView {
        id: mainflick
        anchors.fill: parent
        section.property: "category"
        section.criteria: ViewSection.FullString

        section.delegate: ListItemHeader {
            title: section
        }

        header: Item {
            width: parent.width
            height: iconTop.height + rulesInfo.height + cardExample.height + cardsInfo.height + units.gu(28)

            UbuntuShape {
                id: iconTop
                width: units.gu(20)
                height: width
                aspect: UbuntuShape.Flat

                anchors{
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    topMargin: units.gu(6)
                }

                source: Image {
                    sourceSize.width: parent.width
                    sourceSize.height: parent.height
                    source: iconAppRute
                }
            }

            Label {
                id: appNameLabel
                anchors.top: iconTop.bottom
                anchors.topMargin: units.gu(4)
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Rock Paper Scissors Lizard Spock")
                font.bold: true
            }

            Label {
                id: appInfo
                anchors.top: appNameLabel.bottom
                anchors.topMargin: units.gu(2)
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Version %1. Source %2").arg("1.0").arg(source)
                onLinkActivated: Qt.openUrlExternally(link)
                linkColor: darkColor
            }

            Label {
                id: licenseInfo
                anchors.top: appInfo.bottom
                anchors.topMargin: units.gu(2)
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Under License %1").arg(license)
                onLinkActivated: Qt.openUrlExternally(link)
                linkColor: darkColor
            }

            Label {
                id: rulesInfo
                anchors.top: licenseInfo.bottom
                anchors.topMargin: units.gu(4)
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width - units.gu(4)
                wrapMode: Text.AlignHCenter
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr('<b>Rules:</b> "Scissors cuts paper, paper covers rock, rock crushes lizard, lizard poisons Spock, Spock smashes scissors, scissors decapitates lizard, lizard eats paper, paper disproves Spock, Spock vaporizes rock, and as it always has, rock crushes scissors.", Sheldon Cooper')
            }

            UbuntuShape {
                id: cardExample
                width: units.gu(10)
                height: units.gu(14)
                aspect: UbuntuShape.Flat

                anchors{
                    horizontalCenter: parent.horizontalCenter
                    top: rulesInfo.bottom
                    topMargin: units.gu(2)
                }

                source: Image {
                    sourceSize.width: parent.width
                    sourceSize.height: parent.height
                    source: "../../assets/card-spock-example.svg"
                }
            }

            Label {
                id: cardsInfo
                anchors.top: cardExample.bottom
                anchors.topMargin: units.gu(2)
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width - units.gu(4)
                wrapMode: Text.AlignHCenter
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr('<b>Cards:</b> The cards are layed out to show the hierarchy of the system: top symbols win to the main symbol. Main symbol win to the bottom symbols')
            }
        }

        model: gameStoriesModel

        delegate: ListItem {
            height: storiesDelegateLayout.height
            divider.visible: false
            highlightColor: selectionColor

            ListItemLayout {
                id: storiesDelegateLayout
                title.text: mainText
                subtitle.text: secondaryText
                ProgressionSlot { name: link !== "" ? "next" : ""}
            }

            onClicked: model.link !== "" ? Qt.openUrlExternally(model.link) : null
        }

        ListModel {
            id: gameStoriesModel

            Component.onCompleted: initialize()

            function initialize() {
                gameStoriesModel.append({ category: i18n.tr("App Development"), mainText: "Joan CiberSheep",
                 secondaryText: "", link: "https://cibersheep.com/" })

                gameStoriesModel.append({ category: i18n.tr("Code Used from"), mainText: "Brian Douglas",
                 secondaryText: "Dice Roller (GPLv3)", link: "https://gitlab.com/bhdouglass/dice-roller" })

                gameStoriesModel.append({ category: i18n.tr("Game based on work from"), mainText: "En Tòfol",
                 secondaryText: i18n.tr("designer of the cards and extended rules"), link: "https://labsk.net/index.php?topic=172107.0" })
                gameStoriesModel.append({ category: i18n.tr("Game based on work from"), mainText: "Sam Kass and Karen Bryla",
                 secondaryText: i18n.tr("design of the extended game"), link: "http://www.samkass.com/theories/RPSSL.html" })
                gameStoriesModel.append({ category: i18n.tr("Game based on work from"), mainText: "Ward_Nox",
                 secondaryText: i18n.tr("layout of the card games"), link: "https://www.instructables.com/id/ROCK-PAPER-SISSORS-LIZARD-SPOCK-the-card-game/" })
                gameStoriesModel.append({ category: i18n.tr("Game based on work from"), mainText: "DMacks",
                 secondaryText: i18n.tr("diagram of symbol hierarchy"), link: "https://en.wikipedia.org/wiki/Rock%E2%80%93paper%E2%80%93scissors#/media/File:Pierre_ciseaux_feuille_l%C3%A9zard_spock_aligned.svg" })

                gameStoriesModel.append({ category: i18n.tr("Translations"), mainText: "Anne Onyme 017 ",
                 secondaryText: i18n.tr("French translation"), link: "https://gitlab.com/Anne17" })

                gameStoriesModel.append({ category: i18n.tr("Icons"), mainText: i18n.tr("App Icon"),
                 secondaryText: "CC0 Icons8", link: "https://thenounproject.com/visualpharm/" })
                gameStoriesModel.append({ category: i18n.tr("Icons"), mainText: i18n.tr("Symbol Icons"),
                 secondaryText: "CC-By Studio Fibonacci", link: "https://thenounproject.com/StudioFibonacci/" })
                gameStoriesModel.append({ category: i18n.tr("Icons"), mainText: i18n.tr("Arrow Icon"),
                 secondaryText: "CC-By Kris Prepiakova", link: "https://thenounproject.com/prepiakova/" })
            }
        }
    }
}
