/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

import QtSensors 5.0

Page {
    id: mainPage

    property bool isRolling: false
    signal roll

    anchors.fill: parent

    header: PageHeader {
        id: mainHeader
        title: i18n.tr('Rock Paper Scissors Lizard Spock')

        trailingActionBar {

            actions: Action {
                id: actionInfo
                iconName: "info"
                shortcut: "Ctrl+i"
                text: i18n.tr("Information")
                onTriggered: mainStack.push(Qt.resolvedUrl("PageAbout.qml"));
            }
        }

        extension: Sections {
            width: parent.width
            actions: [
                Action {
                    text: i18n.tr("1 player")

                    onTriggered: soloPlayerView.active = true;
                },
                Action {
                    text: i18n.tr("2 players")

                    onTriggered: soloPlayerView.active = false;
                }
            ]

            Rectangle {
                z: -100
                anchors.fill: parent
                color: theme.palette.normal.background
            }
        }
    }

    SensorGesture {
        id: sensorGesture
        enabled: true
        gestures : ["QtSensors.shake"]
        onDetected: roll()
    }

    Loader {
        id: soloPlayerView
        active: true
        sourceComponent: soloPlayer
        width: parent.width
        height: parent.height - mainHeader.height

        anchors {
            centerIn: parent
            verticalCenterOffset: mainHeader.height / 2
        }
    }

    Loader {
        id: twoPlayerView
        active: !soloPlayerView.active
        sourceComponent: twoPlayer
        width: parent.width
        height: parent.height - mainHeader.height

        anchors {
            centerIn: parent
            verticalCenterOffset: mainHeader.height / 2
        }
    }

    Component {
        id: soloPlayer

        Card {
            id: card1
        }
    }

    Component {
        id: twoPlayer

        Flow {
            anchors {
                fill: parent
            }

            Card {
                id: card1
                width: isLandscape
                    ? twoPlayerView.width / 2
                    : twoPlayerView.width
                height: isLandscape
                    ? twoPlayerView.height
                    : twoPlayerView.height / 2
            }

            Card {
                id: card2
                width: isLandscape
                    ? twoPlayerView.width / 2
                    : twoPlayerView.width
                height: isLandscape
                    ? twoPlayerView.height
                    : twoPlayerView.height / 2
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        enabled: !isRolling
        onClicked: {
            isRolling = true;
            roll();
        }
    }
}
