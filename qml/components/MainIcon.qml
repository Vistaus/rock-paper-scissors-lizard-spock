/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3

UbuntuShape {
    property string mainIcon
    property string mainColor
    width: parent.width
    height: width
    aspect: UbuntuShape.Flat

    source: Image {
        width: parent.width
        height: parent.height
        sourceSize.width: parent.width > units.gu(128)
            ? units.gu(512)
            : parent.width < units.gu(64)
                ? units.gu(32)
                : units.gu(128)
        sourceSize.height: parent.width > units.gu(128)
            ? units.gu(512)
            : parent.width < units.gu(64)
                ? units.gu(32)
                : units.gu(128)
        source: mainIcon
    }
}
