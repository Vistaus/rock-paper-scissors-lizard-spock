[![OpenStore](https://open-store.io/badges/ca.svg)](https://open-store.io/app/rpsls.cibersheep)

# Rock Paper Scissors Lizard Spock

Play this classic game expanded.

"Scissors cuts paper, paper covers rock, rock crushes lizard, lizard poisons Spock, Spock smashes scissors, scissors decapitates lizard, lizard eats paper, paper disproves Spock, Spock vaporizes rock, and as it always has, rock crushes scissors.", Sheldon Cooper

Tap or shake the device to roll, choose 1 or 2 players.

## License

Copyright (C) 2019  Joan CiberSheep

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
